import configparser
from datetime import datetime
import re
import pathlib
import tempfile
import logging

MIDNIGHT = 24 * 60
FILENAME = pathlib.Path("nastavitve.ini")


def must_be_boolean(value):
    if value not in ("ne", "da"):
        return False, "Pricakuje se vrednost da/ne, vrednost je '{}'".format(value)
    else:
        return True, {"da": True, "ne": False}[value]


def must_be_number(value):
    if not re.match(r"\d+\.?\d*$", value):
        return False, "Pricakuje se stevka, dobil '{}'".format(value)
    else:
        return True, float(value)


def folder_must_exist(value):
    if not pathlib.Path(value).exists():
        return False, "Ne najdem mape '{}'".format(value)
    else:
        return True, value


def time_intervals(value):
    intervals = []
    rgx = re.compile(r"(\d\d?):(\d\d?)\-(\d\d?):(\d\d?)")
    for interval in value.split(","):
        interval = interval.replace(" ", "")
        match = rgx.match(interval)
        if not match:
            return False, "Nejasen casovni interval '{}' iz vrednost '{}'".format(interval, value)
        else:
            from_hour, from_min, to_hour, to_min = match.groups()
            from_hour, from_min, to_hour, to_min = int(from_hour), int(from_min), int(to_hour), int(to_min)

            if 0 <= from_hour <= 24 and 0 <= to_hour <= 24 and 0 <= from_min <= 59 and 0 <= to_min <= 59:
                from_min = from_hour * 60 + from_min
                to_min = to_hour * 60 + to_min

                if from_min > MIDNIGHT or to_min > MIDNIGHT:
                    return False, "Cas nastavljen cez polnoc za casovni interval '{}'".format(value)
                elif from_min <= to_min:
                    intervals.append((from_min, to_min))
                else:
                    intervals.append((from_min, MIDNIGHT))
                    intervals.append((0, to_min))
            else:
                return False, "Napacen cas 00:00-23:59 za casovni interval '{}' iz vrednost '{}'".format(interval, value)

    current_time = datetime.now()
    current_min = current_time.hour * 60 + current_time.minute

    result = False
    for from_min, to_min in intervals:
        if from_min <= current_min < to_min:
            result = True
            break

    return True, result


class Config:
    def __init__(self, sysconfig):
        self._config = {}

    def load_value(self, check, check_func, section, key, default):
        if section not in self._config:
            status, value = False, "Ne najdem sekcije '{}' pri nalaganju kljuca '{}'".format(section, key)
        elif key not in self._config[section]:
            status, value = False, "Ne najdem kljuca '{}' v sekciji '{}'".format(key, section)
        else:
            status, value = check_func(self._config[section][key].strip())

        if check:
            return [] if status else ["[{}]{}: {}".format(section, key, value)]
        elif status:
            return value
        else:
            return default

    def music_shuffle(self, check=False):
        return self.load_value(check, must_be_boolean, "GLASBA", "premesaj", False)

    def music_repeat(self, check=False):
        return self.load_value(check, must_be_boolean, "GLASBA", "ponovi", True)

    def music_enabled(self, check=False):
        return self.load_value(check, time_intervals, "GLASBA", "omogoci_ure", True)

    def music_play(self, check=False):
        return self.load_value(check, must_be_boolean, "GLASBA", "predvajaj", False)

    def item_show_time(self, check=False):
        return self.load_value(check, must_be_number, "SLIKA", "cas_prikazovanja", 5)

    def video_show_camera(self, check=False):
        return self.load_value(check, must_be_boolean, "SLIKA", "prikazi_kamero", True)

    def video_enabled(self, check=False):
        return self.load_value(check, time_intervals, "SLIKA", "omogoci_video_ure", True)

    def sleep_enabled(self, check=False):
        return self.load_value(check, time_intervals, "SISTEM", "spanje_ure", False)

    def update(self, locked_remote):
        config = configparser.ConfigParser()

        with tempfile.TemporaryFile() as config_file:
            try:
                locked_remote.download_file(FILENAME, config_file)
            except (TimeoutError, ConnectionError):
                logging.warning("Unable to download config file ({})".format(FILENAME))
                logging.warning("Continue with default config file")
                with open(FILENAME, 'r') as fp:
                    config_file.write(fp.read().encode("UTF-8"))
                    config_file.seek(0)

            data = config_file.read()

        if len(data) == 0:
            logging.warning("Config file size is 0")
            return

        config.read_string(data.decode("UTF-8"))
        self._config = config

        getters = [
            self.music_shuffle, self.music_repeat, self.music_play, self.item_show_time,
            self.video_show_camera, self.video_enabled, self.sleep_enabled]

        for func in getters:
            for err in func(True):
                logging.warning(err)
