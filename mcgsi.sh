#!/bin/bash

cd "$(dirname "$0")"

LOCAL_CONFIG="gsimm.local.json"
DEFAULT_CONFIG="gsimm.json"

JSON_CONFIG=$(test -f $LOCAL_CONFIG && echo "$LOCAL_CONFIG" || echo "$DEFAULT_CONFIG")
python3 main.py $JSON_CONFIG
