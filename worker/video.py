from worker.player import VideoPlayer
from worker.worker import Worker
from worker.rtsp import Rtsp

import pathlib
import time
import logging
import tempfile
import base64
from datetime import datetime
from io import BytesIO

import fitz
from PIL import Image, ImageOps
from screeninfo import get_monitors
from gui import TkWindow


FOLDER = "slika"
DEFAULT_IMAGE = "/tmp/mcgsi-default.jpeg"
IMAGE_FILES = (".jpg", ".jpeg", ".png")
FREQUENCY = 1


class Video(Worker):
    def __init__(self, sysconfig, config):
        super().__init__(FREQUENCY)

        self.player = None
        self.window = TkWindow.create()

        self.files = []
        self.files_index = 0

        self.playing_img = False
        self.last_file_played = 0

        self.rtsp = None
        self.rtsp_playing = False

        self.width = get_monitors()[0].width
        self.height = get_monitors()[0].height

        self.load_config(config)

        with open(DEFAULT_IMAGE, 'wb') as fp:
            fp.write(bytes(base64.b64decode(sysconfig.default_png)))
        self.show_default_image()

    def load_config(self, config):
        self.frequency = config.item_show_time()
        self.video_enabled = lambda: config.video_enabled()
        self.sleeping = lambda: config.sleep_enabled()

    def has_nonvideo_files(self):
        return any((not Video.is_video(f)) for f in self.files)

    def _play_video(self, file_to_play):
        self.player.play_one(file_to_play)
        self.window.show_video()
        self.playing_img = False

    def load_next(self):
        file_to_play = self.files[self.files_index]
        self.files_index = (self.files_index + 1) % len(self.files)

        if Video.is_picture(file_to_play):
            image_data = self.window.get_image_data(file_to_play)
            self.player.stop()
            self.window.show_image(image_data)
            self.playing_img = True
        elif not self.video_enabled():
            if self.has_nonvideo_files():
                self.load_next()
        else:
            self._play_video(file_to_play)

        self.last_file_played = time.time()

    def iter(self, _config):
        if self.rtsp.is_online():
            if not self.rtsp_playing:
                logging.info("Starting rtsp stream")
                time.sleep(2)
                self._play_video(self.rtsp.get_address())
                self.rtsp_playing = True
                self.files_index = 0
                time.sleep(2)

        elif self.rtsp_playing:
            self.rtsp_playing = False
            self.iter(_config)

        elif self.sleeping() or len(self.files) == 0:
            self.files_index = 0
            self.show_default_image()

        elif self.playing_img:
            if time.time() - self.last_file_played > self.frequency:
                self.load_next()

        elif not self.player.is_playing():
            self.load_next()

    def is_playing_video(self):
        return not self.playing_img

    @staticmethod
    def is_video(filepath):
        return filepath.suffix in (".mp4", ".avi", ".mkv", ".m4v")

    @staticmethod
    def is_picture(filepath):
        return filepath.suffix in IMAGE_FILES

    @staticmethod
    def is_pdf(filepath):
        return filepath.suffix in (".pdf",)

    def show_default_image(self, error_msg=None):
        self.window.show_image(self.window.get_image_data(DEFAULT_IMAGE))

        if error_msg is not None:
            self.window.show_error("{}: {}".format(datetime.now(), error_msg))
        self.playing_img = True

    def load_files(self, files):
        self.files = files
        self.files_index = 0

    def register_other_workers(self, workers):
        for worker in workers:
            if isinstance(worker, Rtsp):
                self.rtsp = worker
            elif isinstance(worker, VideoPlayer):
                self.player = worker
                self.player.set_loop(False)
                self.player.set_canvas(self.window.get_video_panel_id())

    def ready_image_for_screen(self, image):
        # first resize
        ratio_screen = self.width / self.height # 800x600 -> 1.25
        ratio_image = image.width / image.height # 1000x500/500x1000 -> 2/0.5

        # 800/300
        image_width = int(self.width if ratio_screen < ratio_image else self.height * ratio_image)
        # 400/600
        image_height = int(self.height if ratio_screen > ratio_image else self.width / ratio_image)

        image_2 = image.resize((image_width, image_height))
       
        # second, put image on screen sized canvas
        left = (self.width - image_width) // 2
        top = (self.height - image_height) // 2

        image_3 = Image.new(image_2.mode, (self.width, self.height), (255, 255, 255))
        image_3.paste(image_2, (left, top))

        return image_3

    def download_files(self, files, locked_remote, tmp_folder):
        video_files = []
        for (filepath, size) in files:
            if FOLDER not in filepath.parts:
                continue

            if Video.is_pdf(filepath):
                temp_filename = tempfile.TemporaryFile()
                locked_remote.download_file(filepath, temp_filename)
                video_files.append((filepath, temp_filename))
            elif Video.is_picture(filepath) or Video.is_video(filepath):
                video_files.append((filepath, None))

        if len(video_files) == 0:
            return []

        video_files = sorted(video_files, key=lambda x: str(x[0]).lower())

        disk_files = []
        for filepath, more in video_files:
            if Video.is_pdf(filepath):
                temp_file = more
                doc = fitz.open("pdf", temp_file.read())

                for i, page in enumerate(doc):
                    picname = pathlib.Path("{}/{}.{}.jpeg".format(tmp_folder, filepath, i))
                    logging.info("Converting page from {} into {}".format(filepath, picname))

                    img_fitz = page.getPixmap(matrix=fitz.Matrix(2, 2)) # default 72dpi makes the image to small and then blurry, matrix->2xZOOM
                    img = Image.frombytes("RGB", [img_fitz.width, img_fitz.height], img_fitz.samples)
                    img_ready = self.ready_image_for_screen(img)

                    picname.parent.mkdir(parents=True, exist_ok=True)
                    img_ready.save(picname, format='JPEG', subsampling=0, quality=95)

                    disk_files.append(picname)

                temp_file.close()

            else:
                image_bytes = BytesIO()
                locked_remote.download_file(filepath, image_bytes)

                image_bytes.seek(0)
                img = Image.open(image_bytes)
                img = ImageOps.exif_transpose(img)
                img_ready = self.ready_image_for_screen(img)

                fileloc = tmp_folder.joinpath(filepath)
                fileloc.parent.mkdir(parents=True, exist_ok=True)
                img_ready.save(fileloc, format='JPEG', subsampling=0, quality=95)
                
                disk_files.append(fileloc)

        return disk_files
