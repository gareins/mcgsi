from worker.worker import Worker

import logging
import socket
from urllib.parse import urlparse

FREQUENCY = 15


class Rtsp(Worker):
    def __init__(self, sysconfig):
        super().__init__(FREQUENCY)
        self.url = sysconfig.rtsp_url
        self.online = False

    def iter(self, _config):
        urlparsed = urlparse(self.url)
        hostname, port = urlparsed.netloc.split(":")

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.5)

        try:
            result = sock.connect_ex((hostname, int(port)))
            self.online = result == 0
        except Exception as e:
            self.online = False

        logging.info("RTSP check on: {}:{} returned {}".format(hostname, port, self.online))

    def is_online(self):
        return self.online

    def register_other_workers(self, workers):
        pass

    def get_address(self):
        return self.url
