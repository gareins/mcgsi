import logging
import tempfile
import pathlib
from utils import PermanentFolder

from worker.worker import Worker
from worker.video import Video
from worker.music import Music

FREQUENCY = 15
OK_FILE = pathlib.Path("ok")


class Downloader(Worker):
    def __init__(self, sysconfig, remote_interface):
        super().__init__(FREQUENCY)

        self.sysconfig = sysconfig
        self.remote_interface = remote_interface
        self.download_location = None
        self.has_successully_update_before = False
        self.video = None
        self.music = None

    def has_ok_file(self, files):
        for filepath, _ in files:
            # todo: only ok in correct dir
            if filepath == OK_FILE:
                return True
        return False

    def register_other_workers(self, workers):
        for worker in workers:
            if isinstance(worker, Video):
                self.video = worker
            elif isinstance(worker, Music):
                self.music = worker

    def iter(self, config):
        logging.info("remote check")

        with self.remote_interface.acquire_locked_remote() as locked_remote:
            files = locked_remote.list_files()
            if (not self.has_ok_file(files)) or not self.has_successully_update_before:
                self.remote_full_update(files, config, locked_remote)

    def remote_full_update(self, remote_files, config, locked_remote):
        logging.info("Full remote update")
        config.update(locked_remote)

        with tempfile.TemporaryDirectory() as tempdirpath:
            download_location = pathlib.Path(tempdirpath)

            logging.info("Downloading video")
            self.video.load_config(config)
            video_files = self.video.download_files(remote_files, locked_remote, download_location)

            logging.info("Downloading audio")
            self.music.load_config(config)
            music_files = self.music.download_files(remote_files, locked_remote, download_location)

            logging.info("All downloaded, upload ok file")
            locked_remote.upload_string("", OK_FILE)

            logging.info("Cleanup old download dir")
            if self.download_location is not None:
                self.download_location.cleanup()

            logging.info("Permanently store and use new files")
            self.download_location = PermanentFolder.from_temporary(
                download_location, (music_files, video_files))

            self.music.load_files(music_files)
            self.video.load_files(video_files)

            self.has_successully_update_before = True
            logging.info("Should clean up: " + str(download_location))
