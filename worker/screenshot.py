import subprocess
import logging
import pathlib
from datetime import datetime

from worker.worker import Worker


UPLOADED_IMAGE_NAME = pathlib.Path("posnetek_zaslona.jpeg")
TEMP_FILE_XWD = "/tmp/mcgsi.xwd"
TEMP_FILE_JPG = "/tmp/mcgsi.jpeg"
QUALITY_JPG = "20"
TEXT_SIZE = "50"
FREQUENCY = 180  # one per 3 min


class Screenshot(Worker):
    def __init__(self, remote_interface):
        super().__init__(FREQUENCY)
        self.remote_interface = remote_interface

    def iter(self, _config):
        self.take_screenshot()

    def take_screenshot(self):
        command1 = "xwd -out {} -root -display :0.0".format(TEMP_FILE_XWD)
        if 0 != subprocess.call(command1.split()):
            logging.warn("Unable to take screenshot")
            return

        localtimestr = datetime.now().isoformat()
        draw = "text 0 {} \"{}\"".format(TEXT_SIZE, localtimestr)
        command2 = [
            "convert",
            "-quality", QUALITY_JPG,
            "-pointsize", TEXT_SIZE, "-fill", "red", "-draw", draw,
            TEMP_FILE_XWD, TEMP_FILE_JPG]

        if 0 != subprocess.call(command2):
            logging.warn("Unable to convert screenshot")
            return

        with open(TEMP_FILE_JPG, 'rb') as fp:
            with self.remote_interface.acquire_locked_remote() as locked_remote:
                locked_remote.upload_bytes(fp.read(), UPLOADED_IMAGE_NAME)

    def register_other_workers(self, workers):
        pass
