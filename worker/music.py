from worker.player import MusicPlayer
import random

from worker.worker import Worker
from worker.video import Video

FREQUENCY = 1
FOLDER = "glasba"


class Music(Worker):
    def __init__(self, sysconfig, config):
        super().__init__(FREQUENCY)

        self.player = None
        self.video = None
        self.load_config(config)

    def load_config(self, config):
        self.shuffle = config.music_shuffle()
        self.repeat = config.music_repeat()
        self.sleeping = lambda: config.sleep_enabled()
        self.enabled = lambda: (config.music_enabled() and config.music_play())

    def load_files(self, files):
        self.player.stop()
        self.player.set_loop(self.repeat)
        if self.shuffle:
            random.shuffle(files)
        self.player.play(files)

    def register_other_workers(self, workers):
        for worker in workers:
            if isinstance(worker, Video):
                self.video = worker
            if isinstance(worker, MusicPlayer):
                self.player = worker

    def download_files(self, files, locked_remote, tmp_folder):
        music_files = []
        for (filepath, size) in files:
            if FOLDER in filepath.parts and filepath.suffix == ".mp3":
                music_files.append(filepath)

        if len(music_files) == 0:
            return []

        local_files = []
        for filepath in music_files:
            local_file = tmp_folder.joinpath(filepath)
            local_file.parent.mkdir(parents=True, exist_ok=True)
            local_files.append(local_file)
            with open(local_file, 'wb') as fp:
                locked_remote.download_file(filepath, fp)

        return local_files

    def iter(self, _config):
        should_pause = self.video.is_playing_video() or self.sleeping() or not self.enabled()
        self.player.set_pause(should_pause)
