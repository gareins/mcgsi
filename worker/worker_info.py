from worker.worker import Worker, workers_info
import logging

FREQUENCY = 120


class WorkerInfo(Worker):
    def __init__(self):
        super().__init__(FREQUENCY)

    def iter(self, _config):
        info_copy = dict(workers_info)
        for info, count in info_copy.items():
            logging.info("{:20s}{}".format(info, count))

    def register_other_workers(self, workers):
        pass
