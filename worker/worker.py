from threading import Thread
import time
import logging
from collections import defaultdict

workers_info = defaultdict(lambda: [0, 0])


class Worker:
    def __init__(self, frequency):
        self.last_time = 0
        self.work_thread = None
        self.worker_frequency = frequency

    def name(self):
        return self.__class__.__name__

    def needs_to_work(self, *args, **kwargs):
        if self.work_thread is not None:
            if self.work_thread.is_alive():
                return False
            else:
                self.work_thread = None

        if time.time() - self.last_time > self.worker_frequency:
            return True

    def work(self, *args):
        if self.needs_to_work():
            self.work_thread = Thread(target=lambda: self.run(*args), name=self.name())
            self.work_thread.start()

    def run(self, *args):
        workers_info[self.name()][0] += 1
        self.last_time = time.time()
        try:
            self.iter(*args)
        except (TimeoutError, ConnectionError) as e:
            logging.info("Worker error in {}: '{}'".format(self.name(), e))

        self.last_time = time.time()
        workers_info[self.name()][1] += 1

    def iter(*args, **kwargs):
        raise NotImplementedError("Iter needs to be implemented")

    def register_other_workers(self, workers):
        raise NotImplementedError("register-other-workers needs to be implemented")
