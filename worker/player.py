import vlc
from worker.worker import Worker

import logging
import ctypes
import subprocess


FREQUENCY = 1


class Player(Worker):
    VLC_WARNING_FIXED = False

    def __init__(self):
        super().__init__(FREQUENCY)

        self.fix_vlc_warning()

        self.player = vlc.MediaPlayer()
        self.player.audio_set_volume(50)
        self.pause = False

        self.play_next_file = False
        self.next_files = []
        self.actively_paused = False

        self.loop = False

    @staticmethod
    def fix_vlc_warning():
        if not Player.VLC_WARNING_FIXED:
            logging.info("Find X11 library")
            command = "ldconfig --print | grep libX11.so | head -n 1 | tr -d '\t' | cut -d ' ' -f1"
            library = subprocess.check_output(command, shell=True).decode().strip()
            logging.info("X11 load threads")
            ctypes.cdll.LoadLibrary(library).XInitThreads()
            Player.VLC_WARNING_FIXED = True
            logging.info("X11 set up correctly")

    def set_canvas(self, hwnd):
        self.player.set_xwindow(hwnd)

    def play(self, next_files):
        self.next_files = next_files
        self.play_next_file = True
        self.set_pause(False)

    def play_one(self, next_file):
        self.play([next_file])

    def set_pause(self, pause):
        self.pause = pause

    def stop(self):
        self.player.stop()

    def set_loop(self, loop):
        self.loop = loop

    def _next_file(self):
        if len(self.next_files) == 0:
            return None

        next_file = self.next_files.pop(0)
        if self.loop:
            self.next_files.append(next_file)

        return next_file

    def _vlc_play(self, file):
        if file is None:
            return

        media = vlc.Media(file)
        self.player.set_media(media)
        self.player.play()

    def iter(self, config):
        if self.pause:
            if self.is_playing():
                self.actively_paused = True
                self.player.pause()
        elif self.actively_paused:
            self.player.play()
            self.actively_paused = False
        elif self.play_next_file:
            self._vlc_play(self._next_file())
            self.play_next_file = False
        elif not self.player.is_playing():
            self._vlc_play(self._next_file())

    def is_playing(self):
        return self.player.is_playing()

    def register_other_workers(self, workers):
        pass


class MusicPlayer(Player):
    pass


class VideoPlayer(Player):
    pass

