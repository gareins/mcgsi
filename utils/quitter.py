class _Quitter:
    def __init__(self):
        self.quit = False

    def do_quit(self):
        self.quit = True

    def should_quit(self):
        return self.quit
