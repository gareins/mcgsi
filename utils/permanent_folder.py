import logging
import pathlib
import shutil
import tempfile
import errno


# still in /tmp, but temporary for program execution time
class PermanentFolder:
    def __init__(self):
        self._dir = tempfile.TemporaryDirectory()

    def path(self):
        return pathlib.Path(self._dir.name)

    @staticmethod
    def from_temporary(tempdir, file_lists):
        from_dir = str(tempdir)

        # https://stackoverflow.com/a/68509894
        result = PermanentFolder()
        to_dir = str(result.path()) + "/f"

        # copy files
        shutil.copytree(from_dir, to_dir, copy_function=shutil.move)

        # mark copied files in lists
        for file_list in file_lists:
            for i in range(len(file_list)):
                file_list[i] = pathlib.Path(str(file_list[i]).replace(from_dir, to_dir))

        return result

    def cleanup(self):
        self._dir.cleanup()

        try:
            shutil.rmtree(self.path())
        except OSError as exc:
            if exc.errno != errno.ENOENT:
                logging.warn("Unable to clear old download location")

        self._dir = None
