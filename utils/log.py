import logging
import sys


def init_logging():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s :: %(levelname)s :: %(message)s', stream=sys.stdout)
