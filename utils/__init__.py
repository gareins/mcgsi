__all__ = [
    "init_logging", "quitter", "PermanentFolder"
]

from utils.log import init_logging
from utils.quitter import _Quitter
from utils.permanent_folder import PermanentFolder

quitter = _Quitter()
