import threading
import inspect

LOGGING_ENABLED = False
COMMON_LOCK = threading.Lock()
TIMEOUT = 1


def lock_logging(msg):
    if LOGGING_ENABLED:
        import logging

        info = ":("
        for frame in inspect.stack():
            if "/remote/" in frame.filename:
                continue
            else:
                info = frame.filename.split("/")[-1] + ":" + str(frame.lineno)
                break
        logging.info("LOCK\t\t\t{}\t{}".format(msg, info))


def check_if_lock_acquired(func):
    def inner(*args, **kwargs):
        self = args[0]

        if not self.tried_to_acquire:
            raise PermissionError("Use with statement to acquire locked remote")

        if not self.acquired():
            raise TimeoutError("Lock not acquired for accessing remote")

        return self.remote._handle_errors(func, *args, **kwargs)

    return inner


class AcquiredRemote:
    def __init__(self, remote):
        self._lock = COMMON_LOCK
        self.timeout = TIMEOUT
        self._acquired = False
        self.remote = remote
        self.tried_to_acquire = False

    def __enter__(self):
        self.tried_to_acquire = True
        lock_logging("W")
        self._acquired = self._lock.acquire(timeout=self.timeout)
        lock_logging("A=" + str(self.acquired()))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.acquired():
            lock_logging("R")
            self._lock.release()

    def acquired(self):
        return self._acquired

    # returns [(pathlib.Path, number),...] ; where (path of file on remote, its size)
    @check_if_lock_acquired
    def list_files(self):
        return self.remote._list_files()

    # parameter: pathlib.Path an writable file pointer, writes into fp, no return
    @check_if_lock_acquired
    def download_file(self, filepath, fp):
        return self.remote._download_file(filepath, fp)

    # parameter: content as bytes() object and pathlib.Path for where to write this to
    @check_if_lock_acquired
    def upload_bytes(self, bytes, filepath):
        return self.remote._upload_bytes(bytes, filepath)

    # parameter: pathlib.Path
    @check_if_lock_acquired
    def delete_file(self, filepath):
        return self.remote._delete_file(filepath)

    # parameter: string and pathlib.Path
    @check_if_lock_acquired
    def append_to_log(self, content, filepath):
        return self.remote._append_to_log(content, filepath)

    def upload_string(self, string, filepath):
        self.upload_bytes(string.encode('utf-8'), filepath)
