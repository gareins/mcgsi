import ftplib
import logging
import pathlib
import re
import socket
import io

from remote.remote import Remote

FTP_LIST_REs = [
	re.compile(r"(?P<directory>\S)\S+\s+\d\s+\d+\s+\d+\s+(?P<size>\d+)\s+\S{3}\s+\d+\s+[0-9:]+\s+(?P<name>.*)"),
	re.compile(r"(?P<directory>\S)\S+\s+\d\s+\S+\s+\S+\s+(?P<size>\d+)\s+\S+\s+\d+\s+(\d+:)?\d+\s+(?P<name>.*)")]

TIMEOUT = 3


class FTPInterface(Remote):
    def __init__(self, sysconfig):
        super().__init__()

        self.url = sysconfig.ftp_url
        self.port = sysconfig.ftp_port
        self.username = sysconfig.ftp_username
        self.password = sysconfig.ftp_password
        self.root_folder = pathlib.Path(sysconfig.ftp_folder)
        self.connection = None

    def _connect(self):
        if self.connection is None:
            self.connection = ftplib.FTP()
            self.connection.connect(self.url, port=self.port, timeout=TIMEOUT)
            self.connection.login(user=self.username, passwd=self.password)
            self._go_to_folder("")

    def _go_to_folder(self, folderpath):
        remote_path = str(self._remote_path(folderpath))
        self.connection.cwd(remote_path)

    def _list_files(self):
        folders = [""]
        files = []

        def parse_line(folder, line):
            for rgx in FTP_LIST_REs:
                match = rgx.match(line)
                if match is None:
                    continue
    
                directory = match.group("directory")
                size = match.group("size")
                name = match.group("name")
    
                if directory == "d":
                    folders.append("{}{}/".format(folder, name))
                else:
                    path = pathlib.Path(folder + name)
                    files.append((path, int(size)))
                break

            else:
                if match is None:
                    logging.warn("Unable to parse LIST line: " + line)
                    return

        while len(folders) > 0:
            folder = folders.pop()
            self._go_to_folder(folder)
            self.connection.retrlines("LIST", lambda x: parse_line(folder, x))

        logging.info("Detected {} files in ftp".format(len(files)))
        return files

    def _download_file(self, filepath, fp):
        logging.info("Downloading: {}".format(str(filepath)))
        self._go_to_folder(filepath.parent)
        self.connection.retrbinary("RETR {}".format(filepath.name), fp.write)
        fp.seek(0)

    def _upload_bytes(self, bytes, filepath):
        logging.info("Uploading: {}".format(str(filepath)))
        self._go_to_folder(filepath.parent)
        with io.BytesIO(bytes) as fp:
            self.connection.storbinary("STOR {}".format(filepath.name), fp)

    def _delete_file(self, filepath):
        logging.info("Deleting: {}".format(str(filepath)))
        self._go_to_folder(filepath.parent)
        self.connection.delete(filepath.name)

    def _append_to_log(self, content, filepath):
        self._go_to_folder(filepath.parent)
        with io.BytesIO(content.encode("utf-8")) as fp:
            self.connection.storbinary("APPE {}".format(filepath.name), fp)

    def _handle_errors(self, func, *args, **kwargs):
        error = None
        try:
            self._connect()
            return func(*args, **kwargs)
        except socket.timeout:
            error = TimeoutError("FTP timed out")
        except socket.gaierror:
            error = ConnectionError("FTP unable to connect")
        except ftplib.error_temp:
            error = ConnectionError("FTP temporary problem")
        except Exception as e:
            error = e

        self.connection = None
        raise error
