import s3fs
import aiohttp

import pathlib
import logging

from remote.remote import Remote
from remote.lock_with_timeout import lock

s3fs.S3FileSystem.read_timeout = 1
s3fs.S3FileSystem.connect_timeout = 1
READ_SIZE = 65536


def errors_handled(func):
    def decorator(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except aiohttp.client_exceptions.ClientConnectorError as e:
            logging.warn("connection error: " + str(e))

    return decorator


class S3Interface(Remote):
    def __init__(self, sysconfig):
        super().__init__()

        self.connection = s3fs.S3FileSystem(client_kwargs={'endpoint_url': sysconfig.s3_url})
        self.root_folder = pathlib.Path("{}/{}".format(sysconfig.s3_bucket, sysconfig.tvname))

    @lock
    def list_files(self):
        result = []
        for parent, folder, files in self.connection.walk(self.root_folder):
            for file in files:
                path = pathlib.Path("{}/{}".format(parent, file))
                size = self.connection.size(str(path))
                result.append((path, size))

        return result

    @lock
    def download_file(self, filepath, fp):
        path = self._remote_path(filepath)
        logging.info("Downloading: " + str(path))

        with self.connection.open(path, 'rb') as fpread:
            while True:
                content = fpread.read(READ_SIZE)
                fp.write(content)
                if len(content) == 0:
                    break
        fp.seek(0)

    @lock
    def upload_bytes(self, bytes, filepath):
        path = self._remote_path(filepath)
        logging.info("Upload to: " + path + ", size " + str(len(bytes)))
        with self.connection.open(path, 'wb') as fp:
            fp.write(bytes)

    @lock
    def delete_file(self, filepath):
        path = self._remote_path(filepath)
        logging.info("Delete " + path)
        self.connection.unlink(path)

    @lock
    def append_to_log(self, content, filepath):
        with self.connection.open(self._remote_path(filepath), 'a') as fp:
            fp.write(content)
