import inspect
from remote.acquired_remote import AcquiredRemote


class Remote:
    def __init__(self):
        self.root_folder = None

    def acquire_locked_remote(self):
        return AcquiredRemote(self)

    def _remote_path(self, filepath):
        return self.root_folder.joinpath(filepath)

    # need to acquire to use these:

    def list_files(self):
        raise PermissionError("Need to acquire remote to before using it")

    def download_file(self, filepath, fp):
        raise PermissionError("Need to acquire remote to before using it")

    def upload_bytes(self, bytes, filepath):
        raise PermissionError("Need to acquire remote to before using it")

    def delete_file(self, filepath):
        raise PermissionError("Need to acquire remote to before using it")

    def append_to_log(self, content, filepath):
        raise PermissionError("Need to acquire remote to before using it")

    def upload_string(self, string, filepath):
        raise PermissionError("Need to acquire remote to before using it")

    # Implement these:

    def _list_files(self):
        raise NotImplementedError("Not implemented: " + inspect.getframeinfo(inspect.currentframe()).function)

    def _download_file(self, filepath, fp):
        raise NotImplementedError("Not implemented: " + inspect.getframeinfo(inspect.currentframe()).function)

    def _upload_bytes(self, bytes, filepath):
        raise NotImplementedError("Not implemented: " + inspect.getframeinfo(inspect.currentframe()).function)

    def _delete_file(self, filepath):
        raise NotImplementedError("Not implemented: " + inspect.getframeinfo(inspect.currentframe()).function)

    def _append_to_log(self, content, filepath):
        raise NotImplementedError("Not implemented: " + inspect.getframeinfo(inspect.currentframe()).function)

    def _handle_errors(self, func, *args, **kwargs):
        raise NotImplementedError("Not implemented: " + inspect.getframeinfo(inspect.currentframe()).function)
