from remote.ftp import FTPInterface

from signal import signal, SIGINT
import os
import sys
import subprocess
import selectors
from collections import defaultdict
import time
import pathlib

LOGGING_FREQUENCY = 10
ENVIRONMENT_FLAG = "MCGSI_FORK"
LOG_FILE = pathlib.Path("stdout")
ERR_FILE = pathlib.Path("stderr")


def handletimeout(func):
    def inner(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except TimeoutError:
            self.log[ERR_FILE].append("Remote timeout")
        except ConnectionError:
            self.log[ERR_FILE].append("Remote connection error")

    return inner


class Parent:
    def __init__(self, configfile, command):
        self.remote_interface = FTPInterface(configfile)
        print("Parent Remote initialized")

        self.command = command
        self.already_killed = False
        self.exit_flag = False

        self.last_log_flush_time = 0
        self.log = defaultdict(list)
        self.selector = None

        signal(SIGINT, lambda _signal, _frame: self.sigint_handler())
        print("Parent initialized")

    @handletimeout
    def log_to_remote_and_file(self, content, filename, flush=False):
        with open(filename, 'a') as fp:
            fp.write(content)
            fp.flush()
        self.log[filename].append(content)

        # flush logs online
        if flush or time.time() - self.last_log_flush_time > LOGGING_FREQUENCY:
            with self.remote_interface.acquire_locked_remote() as locked_remote:
                for fname, content_list in self.log.items():
                    locked_remote.append_to_log("".join(content_list), filename)
                    self.log[fname] = []
                self.last_log_flush_time = time.time()

    def sigint_handler(self):
        if self.already_killed:
            sys.exit(1)
        else:
            self.proc.kill()
            self.exit_flag = True

    def safedecode(self, fp):
        try:
            return fp.decode("utf-8")
        except UnicodeDecodeError:
            return "Unable to decode child output"

    def run(self):
        environment = os.environ.copy()
        environment[ENVIRONMENT_FLAG] = ""

        self.proc = subprocess.Popen(["python3"] + self.command,
                                     stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                     env=environment)

        print("Child process started")

        self.selector = selectors.DefaultSelector()
        self.selector.register(self.proc.stdout, selectors.EVENT_READ)
        self.selector.register(self.proc.stderr, selectors.EVENT_READ)

        while not self.exit_flag:
            try:
                self.log_child_process()
            except Exception as e:
                self.log_to_remote_and_file("Parent had an exception!? " + str(e), ERR_FILE)

        stdout = self.proc.stdout.read()
        stdout = self.safedecode(stdout) if stdout else ""

        stderr = self.proc.stderr.read()
        stderr = self.safedecode(stderr) if stderr else ""

        self.log_to_remote_and_file(stdout, LOG_FILE, True)
        self.log_to_remote_and_file(stderr, ERR_FILE, True)

        print("Child process ended")

    def log_child_process(self):
        while not self.exit_flag:
            for key, _ in self.selector.select():
                data = self.safedecode(key.fileobj.read1())
                if not data:
                    self.exit_flag = True
                    break

                if key.fileobj is self.proc.stdout:
                    self.log_to_remote_and_file(data, LOG_FILE)
                else:
                    self.log_to_remote_and_file(data, ERR_FILE)
