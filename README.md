# mcgsi

Multimedijski center za GS Idrija

## Instalacija

To velja za Raspberry pi.

Paketki:

```
apt install python3-pip python3-tk python3-pil.imagetk x11-apps imagemagick swig
pip3 install python-vlc pillow screeninfo ilock
```

### Mupdf

Meni mupdf ne dela pravilno (`apt install libmupdf-dev, pip3 install pymupdf ->  error pri > import fitz`) upostevaj [ta navodila](https://github.com/pymupdf/PyMuPDF/blob/master/installation/ubuntu/ubuntu_pymupdf.sh), samo uporabi verzijo 1.17.0. Kopija navodil:


```
sudo apt install swig

wget https://mupdf.com/downloads/archive/mupdf-1.17.0-source.tar.gz
tar -zxvf mupdf-1.17.0-source.tar.gz

git clone https://github.com/pymupdf/PyMuPDF.git
cd PyMuPDF
git checkout 1.17.0
cd ..

cd mupdf-1.17.0-source
cp ../PyMuPDF/fitz/_config.h include/mupdf/fitz/config.h

export CFLAGS="-fPIC"
make -j4 HAVE_X11=no HAVE_GLFW=no HAVE_GLUT=no prefix=/usr/local
sudo make HAVE_X11=no HAVE_GLFW=no HAVE_GLUT=no prefix=/usr/local install

cd ../PyMuPDF
pip3 install .
```

# Konfiguracija

GPU memorija mora biti zvecana, prav tako si lahko privoscimo da malo znizamo frekvence procesorja, dodaj v /boot/config.txt
Za rpi4 tega nisem spreminjal.

``` 
gpu_mem=256 
arm_freq=1000
gpu_freq=300
```

Da preprecimo timeout zaslona izbrisi screensaver:

```
sudo apt remove xscreensaver
```

In podaj parameter Xserverju, da ne zatemni zaslona, spremeni *pravo* vrstico v datoteki `/etc/lightdm/lightdm.conf`

```
xserver-command=X -s 0 dpms
```

Prav tako je po defaultu HDMI zvok izkljucen, uporabi raspi-config da ga omogocis.

## Autostart

Za avtomaticno poganjanje priporocam uporabo desktop datoteke. Preverite ali je vsebina OK, nato pa jo prenesite na lokacijo `~/.config/autostart`. Po rebootu bi se morala aplikacija zagnati sama.

## Swap

Za zmanjsanje obrabe sd kartice:

* izkljuci swap `swapoff --all`
* izbrisi swap paketek `apt remove dphys-swapfile`
* izbrisi pymupdf in druge datoteke da das sd kartici cim vec prostora
