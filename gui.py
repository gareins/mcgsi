import tkinter as tk
from PIL import ImageTk, Image
import utils

import logging
import threading
import time
import sys

window = None


class TkWindow():
    def __init__(self):
        self.tk = tk.Tk()
        self.tk.attributes('-zoomed', True)
        self.tk.attributes("-fullscreen", True)
        self.tk.config(cursor="none")

        self.tk.bind("<Key>", lambda event: self.key_pressed(event))

        self.rect = (self.tk.winfo_screenwidth(),
                     self.tk.winfo_screenheight())

        # container of two labels
        container = tk.Frame(self.tk)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.video_panel = tk.Label(container)
        self.video_panel.grid(row=0, column=0, sticky="nsew")

        self.image_panel = tk.Label(container, compound="top")
        self.image_panel.grid(row=0, column=0, sticky="nsew")

        self.error_text = None
        self.img_data = None

        self.run_functions = []
        self.update()

    def key_pressed(self, event):
        if event.keysym == "Escape":
            utils.quitter.do_quit()
            self.tk.quit()

    def update(self):
        to_run, self.run_functions = self.run_functions, []
        for func in to_run:
            func()
        self.tk.after(100, self.update)

    def run(self):
        self.tk.mainloop()

    def get_video_panel_id(self):
        return self.video_panel.winfo_id()

    def show_video(self):
        self.hide_error()
        self.run_functions.append(self.video_panel.tkraise)

    def update_image(self):
        def worker():
            self.image_panel.image = self.image_data
            self.image_panel.configure(image=self.image_data)

            if self.error_text is not None:
                self.image_panel.configure(
                    text=self.error_text + "\n", fg="red", font="Arial 40 bold", anchor="e", justify=tk.LEFT)

            self.image_panel.tkraise()

        self.run_functions.append(worker)

    def get_image_data(self, image_path):
        img_pil = Image.open(image_path)
        if img_pil.size != self.rect:
            img_pil = img_pil.resize(self.rect)

        return ImageTk.PhotoImage(img_pil)

    def show_image(self, image_data):
        self.image_data = image_data
        self.error_text = None
        self.update_image()

    def show_error(self, msg):
        self.error_text = msg
        self.update_image()

    def hide_error(self):
        self.error_text = None
        self.update_image()

    @staticmethod
    def _inner_create():
        global window
        window = TkWindow()
        window.run()

    @staticmethod
    def create():
        if window is None:
            threading.Thread(target=TkWindow._inner_create).start()

        for _ in range(20):
            time.sleep(0.1)
            if window is not None:
                return window

        logging.error("Cannot get window :(")
        sys.exit(1)
