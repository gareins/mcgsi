import json
import ctypes
import os
import sys
import logging
import threading
from types import SimpleNamespace
from time import time, sleep

from worker.music import Music
from worker.worker_info import WorkerInfo
from worker.video import Video
# from remote.s3 import S3Interface
from remote.ftp import FTPInterface
from worker.screenshot import Screenshot
from config import Config
from parent import Parent, ENVIRONMENT_FLAG
from worker.downloader import Downloader
from worker.rtsp import Rtsp
from worker.player import MusicPlayer, VideoPlayer
import utils


LOOP_TIME = 1 / 3


class Main(threading.Thread):
    def __init__(self, sysconfig):
        super().__init__()

        self.sysconfig = sysconfig
        self.remote_interface = FTPInterface(sysconfig)

        self.config = Config(sysconfig)
        with self.remote_interface.acquire_locked_remote() as locked_remote:
            self.config.update(locked_remote)

        self.workers = [Video(sysconfig, self.config),
                        Music(sysconfig, self.config),
                        Screenshot(self.remote_interface),
                        Downloader(sysconfig, self.remote_interface),
                        Rtsp(sysconfig),
                        WorkerInfo(),
                        MusicPlayer(),
                        VideoPlayer()]

        for worker in self.workers:
            worker.register_other_workers(self.workers)

        self.errs = []

    def run(self):
        while not utils.quitter.should_quit():
            start_time = time()

            for worker in self.workers:
                worker.work(self.config)

            to_sleep = LOOP_TIME - (time() - start_time)
            if to_sleep > 0:
                sleep(to_sleep)
            else:
                logging.warn("sleep time negative: " + str(to_sleep))


def load_sysconfig(filename):
    with open(filename, 'r') as fp:
        return json.loads(fp.read(), object_hook=lambda d: SimpleNamespace(**d))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        logging.error("No config given as parameter")
        print("No config given as parameter", file=sys.stderr)

    elif ENVIRONMENT_FLAG in os.environ:
        utils.init_logging()

        logging.info("Starting child")
        main = Main(load_sysconfig(sys.argv[1]))

        logging.info("Starting main loop")
        main.start()
        main.join()

    else:
        parent = Parent(load_sysconfig(sys.argv[1]), sys.argv)
        parent.run()
